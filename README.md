# Spreadsheet2JSON Rust

This Rust application shows how to parse a spreadsheet to JSON using the calamine library. It shows how to parse multiple sheets and export it to a nested JSON array.

I was motivated to create this simple application in order to verify my understanding in Rust. The source code contains several basic concepts in Rust programming language, such as: mutability, data types, ownership (reference, cloning and borrowing), the complicated strings concept, collections, and package management.

## Getting Started

``` bash
cargo build
target/debug/spreadsheet2json <filename>
```
