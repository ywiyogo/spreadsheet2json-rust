/**
 * Author: Yongkie Wiyogo
 * Description: Parsing spreadsheet (Excel *.xlsx, LibreOffice *.ods) to JSON
 */
extern crate calamine;
extern crate serde_json;

use calamine::{open_workbook_auto, DataType, Reader};
use serde_json::{json, Value};
use std::collections::HashMap;
use std::env;
use std::fs::OpenOptions;
use std::io::prelude::*;
use std::path::{Path, PathBuf};

// Helper function to copy the table header to a vector list
fn get_header(row: &[DataType], headers: &mut Vec<String>) {
    for cell in row.iter() {
        let cell_value = match cell {
            DataType::String(val) => val.to_string(),
            _ => "".to_string(),
        };
        headers.push(cell_value);
    }
}

fn main() {
    // Get the filename from the command line arguments
    let args: Vec<String> = env::args().collect();
    let filename: &String = &args[1];
    const OUTPUT: &str = "output/result.json";
    if Path::new(OUTPUT).exists() {
        std::fs::remove_file(OUTPUT).unwrap();
    }
    let mut outfile = OpenOptions::new()
        .create_new(true)
        .write(true)
        .append(false)
        .open(OUTPUT)
        .unwrap();

    let path_file: PathBuf = PathBuf::from(&filename);

    // Verify the input filename
    match path_file.extension().and_then(|s| s.to_str()) {
        Some("xlsx") | Some("xlsm") | Some("xlsb") | Some("xls") | Some("ods") => (),
        _ => panic!("Expecting an excel file"),
    }

    // Open the Excel file
    let mut spreadsheets = open_workbook_auto(&path_file).unwrap();

    // End result of the JSON that represented as a root json node with Map type
    let mut root_json: serde_json::Map<String, Value> = serde_json::Map::new();

    let mut location_headers: Vec<String> = vec![];
    let mut locations: Vec<Value> = Vec::new();
    let mut locations_map: HashMap<u32, Value> = HashMap::new();
    // ------------------------------------------------------------
    // Look for the a sheet name "locations" and parse the content
    // ------------------------------------------------------------
    if let Some(Ok(range)) = spreadsheets.worksheet_range("locations") {
        for (i, row) in range.rows().enumerate() {
            // Get first row header by assuming the header on the first row
            if i == 0 {
                get_header(row, &mut location_headers);
            }
            // Parse the content to the vector with JSON objects
            let id_str: &str = "id";
            if !row[0].eq(id_str) {
                let mut json_object = json!({});
                for (col_index, cell) in row.iter().enumerate() {
                    let cell_value = match cell {
                        DataType::Float(val) => val.to_string(),
                        DataType::String(val) => val.to_string(),
                        _ => "".to_string(),
                    };
                    json_object[location_headers[col_index].to_string()] = json!(cell_value);
                    let rangecell = range.get_value((i as u32, 0)).unwrap();
                    let id: u32 = match rangecell {
                        DataType::Float(val) => val.round() as u32,
                        _ => u32::MAX,
                    };
                    locations_map.insert(id, json_object.clone());
                }
                locations.push(json_object.clone());
            }
        }
    }
    // insert the content of the locations sheet to the JSON root
    root_json.insert("locations".to_string(), Value::Array(locations));

    // ------------------------------------------------------------
    // Look for the a sheet name "schedules" and parse the content
    // ------------------------------------------------------------
    let mut schedule_headers: Vec<String> = vec![];
    let mut schedules: Vec<Value> = Vec::new();
    if let Some(Ok(range)) = spreadsheets.worksheet_range("schedules") {
        for (i, row) in range.rows().enumerate() {
            // Get first row header by assuming the header on the first row
            if i == 0 {
                get_header(row, &mut schedule_headers);
            }

            println!("row={:?}, row[0]={:?}", row, row[0]);
            // Skip the first row (header)
            let id_str: &str = "id";
            if !row[0].eq(id_str) {
                let mut json_object = json!({});
                // Create a JSON object for each row including checking the datatype
                for (col_index, cell) in row.iter().enumerate() {
                    let cell_value: String = match cell {
                        DataType::Float(val) => val.to_string(),
                        DataType::String(val) => val.to_string(),
                        // convert the float number to timestamp
                        DataType::DateTime(val) => {
                            let mut hours = val * 24.0;
                            // in case of 11:00
                            if hours % 1.0 > 0.99 {
                                hours = hours.round();
                            }
                            let minutes = (hours % 1.0) * 60.0;
                            // float comparison need a tolerance
                            let epsilon = 0.01;
                            let min_string = if (minutes - 0.0).abs() < epsilon
                                || (minutes - 59.0).abs() < epsilon
                            {
                                "00".to_string()
                            } else {
                                minutes.round().to_string()
                            };
                            hours.floor().to_string() + ":" + &min_string
                        }
                        DataType::Int(val) => val.to_string(),
                        DataType::Bool(val) => val.to_string(),
                        DataType::Error(val) => val.to_string(),
                        DataType::Empty => "".to_string(),
                    };
                    // Add the cell value to the JSON object
                    if schedule_headers[col_index].to_string() == "from".to_string()
                        || schedule_headers[col_index] == "to".to_string()
                    {
                        let location_id = cell_value.parse::<usize>().unwrap() as u32;
                        if location_id < locations_map.len() as u32 {
                            json_object[schedule_headers[col_index].to_string()] =
                                json!(locations_map[&location_id]);
                        } else {
                            eprintln!("Couldn't create a JSON object due to invalid location id: {}, in row {}", location_id, i);
                        }
                    } else {
                        json_object[schedule_headers[col_index].to_string()] = json!(cell_value);
                    }
                }
                schedules.push(json_object.clone());
                // Print the JSON object
                // println!("{},", json_object.to_string());
            }
        }
    }
    // insert the content of the schedules sheet to the JSON root
    root_json.insert("schedules".to_string(), Value::Array(schedules));
    if let Err(e) = writeln!(
        outfile,
        "{}",
        serde_json::to_string_pretty(&root_json).unwrap()
    ) {
        eprintln!("Couldn't write to file: {}", e);
    }
}
